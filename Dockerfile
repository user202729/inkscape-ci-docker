FROM ubuntu:23.10
ENV DEBIAN_FRONTEND=noninteractive
ARG CI
ADD install_dependencies.sh /
RUN /install_dependencies.sh --full
RUN apt install -y libgstreamer-plugins-bad1.0-dev mm-common glslc
RUN rm -rf /install_dependencies.sh /var/lib/apt/lists/*

ARG ver=4.14 pt=0
ARG ver2=$ver.$pt
RUN wget https://download.gnome.org/sources/gtkmm/$ver/gtkmm-$ver2.tar.xz https://download.gnome.org/sources/gtkmm/$ver/gtkmm-$ver2.sha256sum && sha256sum --check --ignore-missing gtkmm-$ver2.sha256sum && rm gtkmm-$ver2.sha256sum && tar xf gtkmm-$ver2.tar.xz && rm gtkmm-$ver2.tar.xz && cd gtkmm-$ver2 && meson setup --libdir lib builddir . && cd builddir && ninja && ninja install && cd ../.. && rm -rf gtkmm-$ver2

ARG ver=2.78 pt=1
ARG ver2=$ver.$pt
RUN wget https://download.gnome.org/sources/glibmm/$ver/glibmm-$ver2.tar.xz https://download.gnome.org/sources/glibmm/$ver/glibmm-$ver2.sha256sum && sha256sum --check --ignore-missing glibmm-$ver2.sha256sum && rm glibmm-$ver2.sha256sum && tar xf glibmm-$ver2.tar.xz && rm glibmm-$ver2.tar.xz && cd glibmm-$ver2 && meson setup --libdir lib builddir . && cd builddir && ninja && ninja install && cd ../.. && rm -rf glibmm-$ver2
